# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://pigi98@bitbucket.org/pigi98/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/pigi98/stroboskop/commits/306cc2253e8f262ada0a3c537af322cd23295339

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/pigi98/stroboskop/commits/07f3987ffd4df30f54215d3b81fa075a2d18851c

Naloga 6.3.2:
https://bitbucket.org/pigi98/stroboskop/commits/8da8c261e0e71b52663ca79c05d663411b2f1b0b

Naloga 6.3.3:
https://bitbucket.org/pigi98/stroboskop/commits/b569b2cec47b2dd44eabeabdd43f14c5a7ee6787

Naloga 6.3.4:
https://bitbucket.org/pigi98/stroboskop/commits/6ca28a2cf92ffa878a18f832b82fd18ee6e3fbca

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/pigi98/stroboskop/commits/9abcb2ef95e0cf8f7902486b7335c57431131162

Naloga 6.4.2:
https://bitbucket.org/pigi98/stroboskop/commits/d1879d43734ddcd447ae2775001f9aed488b5405

Naloga 6.4.3:
https://bitbucket.org/pigi98/stroboskop/commits/94b72aa5e465eef7843abac25f9922cd2e36b975

Naloga 6.4.4:
https://bitbucket.org/pigi98/stroboskop/commits/857918bde250d2d2331a96336d66d42e38a3a0d2